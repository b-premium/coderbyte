/*
Challenge
Using the JavaScript language, have the function LetterCapitalize(str) 
take the str parameter being passed and capitalize the first letter of each word.
 Words will be separated by only one space.

Sample Test Cases
Input:"hello world"

Output:"Hello World"


Input:"i ran there"

Output:"I Ran There"
*/

function capitalizeFirstLetter(str) {
    var charArray = str.split('');
    var output = '';
    for (var i = 0; i < charArray.length; i++) {
        if ( i === 0){
            output += charArray[i].toUpperCase();
        } else {
            output += charArray[i];
        }
    }
    return output;
}

function LetterCapitalize(str) {
    var inputArray = str.split(' ');
    var output = '';
    for (var i=0; i < inputArray.length; i++) {
        var currentString = capitalizeFirstLetter(inputArray[i]);        
        if ( i == inputArray.length - 1) {
            output += currentString;
        } else {
            output += currentString + ' ';
        }
    }
    return output;
}

var output = LetterCapitalize("hello world");
console.log('output=', output);
output = LetterCapitalize("i ran there");
console.log('output=', output);