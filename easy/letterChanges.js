/*

Challenge
Using the JavaScript language, have the function LetterChanges(str)
take the str parameter being passed and modify it using the following algorithm.
 Replace every letter in the string with the letter following it in the alphabet
(ie. c becomes d, z becomes a). Then capitalize every vowel in this new string
(a, e, i, o, u) and finally return this modified string.
Sample Test Cases

Input:"hello*3"

Output:"Ifmmp*3"


Input:"fun times!"

Output:"gvO Ujnft!"


*/

function getMyCharIndex(currentChar) {
    var myCharObject = getCharIndexUsingFilter(currentChar);   
    if ( myCharObject.length === 1){
        return myCharObject[0].index;
    } else {
        return '';
    }
}

function getCharIndexUsingFilter(currentChar) {
    var alphaObject = [{alpha: 'a', index: 0}, {alpha: 'b', index: 1}, {alpha: 'c', index: 2}, {alpha:'d', index: 3}, {alpha:'e', index: 4},
                       {alpha: 'f', index: 5}, {alpha: 'g', index: 6}, {alpha: 'h', index: 7}, {alpha: 'i', index: 8}, {alpha: 'j', index: 9},
                       {alpha:'k', index: 10}, {alpha: 'l', index: 11}, {alpha: 'm', index: 12}, {alpha: 'n', index: 13},
                       {alpha: 'o', index: 14}, {alpha: 'p', index: 15}, {alpha: 'q', index: 16}, {alpha:'r', index: 17}, {alpha:'s', index: 18}, {alpha:'t', index: 19}, {alpha: 'u', index: 20},
                       {alpha: 'v', index: 21}, {alpha: 'w', index: 22}, {alpha: 'x', index: 23}, {alpha: 'y', index: 24}, {alpha: 'z', index: 25}];
   
    return alphaObject.filter( function(currentObject) {
         return currentObject.alpha === currentChar
    });
}

function getNextChar(currentCharIndex) {
    var alphaArray = ['a','b', 'c', 'd','e','f', 'g', 'h',
    'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
    'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    
    if (currentCharIndex === 25) {
        return 'a'
    } else {
        return  alphaArray[currentCharIndex + 1];
    }
}

function letterChanges(str) {   
    var inputArray = str.split('');
    var result = '';
    inputArray.forEach( function(currentChar) {        
        var currentCharIndex = getMyCharIndex(currentChar);        
       if ( typeof currentCharIndex === "number") {
            result += getNextChar(currentCharIndex);
        } else {
            result += currentChar;
        }
    });
    return result;
}

/*
function getCharIndex(char) {
    var alphaObject = [{alpha: 'a', index: 0}, {alpha: 'b', index: 1}, {alpha: 'c', index: 2}, {alpha:'d', index: 3}, {alpha:'e', index: 4},
                       {alpha: 'f', index: 5}, {alpha: 'g', index: 6}, {alpha: 'h', index: 7}, {alpha: 'i', index: 8}, {alpha: 'j', index: 9},
                       {alpha:'k', index: 10}, {alpha: 'l', index: 11}, {alpha: 'm', index: 12}, {alpha: 'n', index: 13},
                       {alpha: 'o', index: 14}, {alpha: 'p', index: 15}, {alpha: 'q', index: 16}, {alpha:'r', index: 17}, {alpha:'s', index: 18}, {alpha:'t', index: 19}, {alpha: 'u', index: 20},
                       {alpha: 'v', index: 21}, {alpha: 'w', index: 22}, {alpha: 'x', index: 23}, {alpha: 'y', index: 24}, {alpha: 'z', index: 25}];
    var result = '';
    alphaObject.map( function(currentObject) {
         if (currentObject.alpha === char) {
            result = currentObject.index;
         }
    });
    return result;
}
*/


var result = letterChanges('ahello*3z');
console.log('result=', result);
