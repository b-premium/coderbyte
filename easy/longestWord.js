/*
Challenge
Using the JavaScript language, have the function LongestWord(sen) take the sen parameter 
being passed and return the largest word in the string. If there are two or more words that are the same length,
return the first word from the string with that length. Ignore punctuation and assume sen will not be empty. 
Sample Test Cases
Input:"fun&!! time"

Output:"time"


Input:"I love dogs"

Output:"love"
*/


function LongestWord(sen) {
    var wordsArray = sen.split(' ');
    var size =  wordsArray.length;
    var longestWordSize= 0;
    var longestWordString = '';

    for(var i in wordsArray){
        var currentWordString = wordsArray[i].replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"");        
        var currentWordSize = currentWordString.length;   
        if (currentWordSize > longestWordSize) {
            longestWordString = currentWordString;
            longestWordSize = currentWordSize;
        }
    }  
    return longestWordString;           
  }

 var result = LongestWord("fun&!! time");
 console.log('result', result);