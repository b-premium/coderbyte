/*
Challenge
Using the JavaScript language, have the function FirstReverse(str) 
take the str parameter being passed and return the string in reversed order. 
For example: if the input string is "Hello World and Coders" 
then your program should return the string sredoC dna dlroW olleH. 
Sample Test Cases
Input:"coderbyte"

Output:"etybredoc"


Input:"I Love Code"

Output:"edoC evoL I"
*/

function FirstReverse(str) {
    var charArray = str.split('');
    var startIndex = charArray.length - 1;
    var reverseString = '';

    for (var i = startIndex; i >= 0; i--) {
        reverseString += charArray[i];
    }
    return reverseString;
}

var result = FirstReverse("coderbyte");
console.log("reverse of 'coderbyte'=", result);
result = FirstReverse("I Love Code");
console.log("reverse of 'I Love Code'=", result);